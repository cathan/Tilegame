#include "include/tile.h"
#include "include/textureHolder.h"

void Tile::create(int xPos, int yPos, int type)
{
    if (type >= 0 && type <= 14) m_Type = type;
    else m_Type = 0;

    if (m_Type < 14) m_Sprite = sf::Sprite(TextureHolder::GetTexture("../resources/graphics/tile.png"));
    else m_Sprite = sf::Sprite(TextureHolder::GetTexture("../resources/graphics/person.png"));

    m_Position.x = xPos;
    m_Position.y = yPos;

    m_Sprite.setPosition(m_Position);
}

sf::FloatRect Tile::getPosition()
{
    return m_Sprite.getGlobalBounds();
}

sf::Sprite Tile::getSprite()
{
    return m_Sprite;
}

void Tile::update()
{
    // Move the tile
    m_Sprite.setPosition(m_Position);

    // Make tile inactive at negative values
    if (m_Type < 7) m_Active = false;
    else m_Active = true;

    // Set to colour based on type
    switch (m_Type)
    {
        case 7:
            m_Sprite.setColor(sf::Color::Red);
            break;

        case 8:
            m_Sprite.setColor(sf::Color::Yellow);
            break;

        case 9:
            m_Sprite.setColor(sf::Color::Green);
            break;

        case 10:
            m_Sprite.setColor(sf::Color::Cyan);
            break;

        case 11:
            m_Sprite.setColor(sf::Color::Blue);
            break;

        case 12:
            m_Sprite.setColor(sf::Color::Magenta);
            break;
    }
}

short Tile::getType()
{
    return m_Type;
}

void Tile::setType(short type)
{
    if(type > 0 && type < 14) m_Type = type;
    else m_Type = 0;
}

bool Tile::isActive()
{
    return m_Active;
}
