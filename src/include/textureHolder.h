//
//  textureHolder.h
//
//  Copyright � 2018 Brendan Lyng. All rights reserved.
//

#ifndef TEXTUREHOLDER_H
#define TEXTUREHOLDER_H

#include <SFML/Graphics.hpp>
#include <map>

class TextureHolder
{
public:
    TextureHolder();
    static sf::Texture& GetTexture(std::string const& filename);

private:
    // A map holding pairs of String and Texture
    std::map<std::string, sf::Texture> m_Textures;

    // A pointer to the singleton
    static TextureHolder* m_s_Instance;
};

#endif // TEXTUREHOLDER_H
