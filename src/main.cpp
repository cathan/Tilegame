#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include "include/textureHolder.h"
#include "include/main.h"
#include "include/tile.h"

void moveTiles(int tile);

int main()
{
    // Create the texture holder
    TextureHolder holder;

    // Game states
    enum class State {MENU, LEVEL_UP, GAME_OVER, PLAYING};
    // Start with the GAME_OVER state
    State state = State::GAME_OVER;

    sf::IntRect field;

    // Create the main window
    sf::RenderWindow window(sf::VideoMode(640, 480), "Tileroom", sf::Style::Titlebar|sf::Style::Close);

    // Create a main SFML View
    sf::View mainView(sf::FloatRect(0, 0, window.getSize().x, window.getSize().y));

    // Create a view for the HUD
    sf::View hudView(sf::FloatRect(0, 0, window.getSize().x, window.getSize().y));

    // Create a view for the menu
    sf::View menuView(sf::FloatRect(0, 0, window.getSize().x, window.getSize().y));

    // Load the font
    sf::Font font;
    font.loadFromFile("../resources/fonts/courbd.ttf");

    // Player score
    int score = 0, hiScore = 0;
    // Score to increment up to (purely aesthetic)
    int targetScore = 0;
    // Combo chain
    short scoreChain = 1;

    // Score text
    sf::Text scoreText;
    scoreText.setFont(font);
    scoreText.setCharacterSize(16);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setPosition(0,0);
    scoreText.setString("Score: ");

    // Score chain text
    sf::Text chainText;
    chainText.setFont(font);
    chainText.setCharacterSize(16);
    chainText.setFillColor(sf::Color::White);
    chainText.setPosition(0,20);
    chainText.setString("Chain: x");

    // Score text background
    sf::RectangleShape scoreRect;
    scoreRect.setFillColor(sf::Color::Black);
    scoreRect.setSize(sf::Vector2f(140,40));
    scoreRect.setPosition(0,0);
    scoreRect.setOutlineColor(sf::Color::White);
    scoreRect.setOutlineThickness(2);

    // Next shape text background
    sf::RectangleShape nextRect;
    nextRect.setFillColor(sf::Color::Black);
    nextRect.setSize(sf::Vector2f(100,100));
    nextRect.setPosition(window.getSize().x - 100,0);
    nextRect.setOutlineColor(sf::Color::White);
    nextRect.setOutlineThickness(2);

    // Game over text
    sf::Text nextText;
    nextText.setFont(font);
    nextText.setCharacterSize(16);
    nextText.setFillColor(sf::Color::White);
    nextText.setPosition(window.getSize().x - 100,0);
    nextText.setString("Next tile: ");

    // Game over text
    sf::Text gameOverText;
    gameOverText.setFont(font);
    gameOverText.setCharacterSize(48);
    gameOverText.setFillColor(sf::Color::White);
    gameOverText.setString("Press Enter to play");

    // Load the high score from a text file
    std::ifstream inputFile("../resources/gamedata/scores.txt");
    if (inputFile.is_open()) {
      inputFile >> hiScore;
      inputFile.close();
    }

    // Game over text
    sf::Text hiScoreText;
    hiScoreText.setFont(font);
    hiScoreText.setCharacterSize(24);
    hiScoreText.setFillColor(sf::Color::White);
    hiScoreText.setString("High score: ");
    hiScoreText.setPosition(0,window.getSize().y - 40);

    sf::FloatRect gameOverRect = gameOverText.getLocalBounds();
    gameOverText.setOrigin(gameOverRect.left+gameOverRect.width/2.0f, gameOverRect.top+gameOverRect.height/2.0f);
    gameOverText.setPosition(window.getSize().x/2, window.getSize().y/2);

    // Pause menu background
    sf::RectangleShape menuRect;
    menuRect.setFillColor(sf::Color::Black);
    menuRect.setSize(sf::Vector2f(300,80));
    menuRect.setPosition((window.getSize().x / 2) - 150,(window.getSize().y / 2) - 40);
    menuRect.setOutlineColor(sf::Color::White);
    menuRect.setOutlineThickness(2);

    // Pause menu text
    sf::Text menuText;
    menuText.setFont(font);
    menuText.setCharacterSize(24);
    menuText.setFillColor(sf::Color::White);
    menuText.setPosition(menuRect.getPosition().x + 70 ,menuRect.getPosition().y);
    menuText.setString("Game paused");

    // Retire button text
    sf::Text quitText;
    quitText.setFont(font);
    quitText.setCharacterSize(24);
    quitText.setFillColor(sf::Color::White);
    quitText.setPosition(menuRect.getPosition().x, menuRect.getPosition().y + 40);
    quitText.setString("(Press enter to quit)");

    // Where is the mouse in relation to world coordinates
    sf::Vector2f mouseWorldPosition;
    // Where is the mouse in relation to screen coordinates
    sf::Vector2i mouseScreenPosition;

    Tile* tiles = nullptr;

    // Size of field, tiles out
    short fieldSize = 0;

    // X and Y position of field on screen
    int xPos = 0;
    int yPos = 0;

    // Next tile
    sf::Sprite nextTile = sf::Sprite(TextureHolder::GetTexture("../resources/graphics/tile.png"));
    nextTile.scale(2,2);
    nextTile.setPosition(window.getSize().x - 80,30);
    short nextTileType = 0;

    // Currently selected tile and sprite
    sf::Texture selectTexture = TextureHolder::GetTexture("../resources/graphics/select.png");
    sf::Sprite pointSprite = sf::Sprite(selectTexture);
    sf::Sprite selectSprite = sf::Sprite(selectTexture);
    selectSprite.setColor(sf::Color(255,255,255,127));

    // Init selection values
    int pointedTile = 0;
    int selectedTile = 0;

    // Prepare the select sounds
    sf::SoundBuffer selectOneBuffer;
    selectOneBuffer.loadFromFile("../resources/audio/select.wav");
    sf::Sound selectOneSound;
    selectOneSound.setBuffer(selectOneBuffer);

    sf::SoundBuffer selectTwoBuffer;
    selectTwoBuffer.loadFromFile("../resources/audio/select2.wav");
    sf::Sound selectTwoSound;
    selectTwoSound.setBuffer(selectTwoBuffer);

    // Prepare the drop sounds
    sf::SoundBuffer dropBuffer;
    dropBuffer.loadFromFile("../resources/audio/drop.wav");
    sf::Sound dropSound;
    dropSound.setBuffer(dropBuffer);

    // Prepare the match sounds
    sf::SoundBuffer matchBuffer;
    matchBuffer.loadFromFile("../resources/audio/match.wav");
    sf::Sound matchSound;
    matchSound.setBuffer(matchBuffer);

    // Prepare the move sounds
    sf::SoundBuffer moveBuffer;
    moveBuffer.loadFromFile("../resources/audio/move.wav");
    sf::Sound moveSound;
    moveSound.setBuffer(moveBuffer);

    // Seed the RNG
    srand((int)time(0));

    // The main game loop
    while (window.isOpen())
    {
        /*
         ************
         Handle input
         ************
         */

        // Handle events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::KeyPressed) {
                // Start a new game while in GAME_OVER state
                if (event.key.code == sf::Keyboard::Return &&
                         state == State::GAME_OVER)
                {
                    state = State::LEVEL_UP;
                }

                // Pause a game while playing
                if (event.key.code == sf::Keyboard::Escape &&
                    state == State::PLAYING)
                {
                    selectOneSound.play();
                    state = State::MENU;
                }
                // Restart while paused
                else if (event.key.code == sf::Keyboard::Escape &&
                         state == State::MENU)
                {
                    selectOneSound.play();
                    state = State::PLAYING;
                }

                // Return to menu
                else if (event.key.code == sf::Keyboard::Return &&
                         state == State::MENU)
                {
                    std::ofstream outputFile("../resources/gamedata/scores.txt");
                        outputFile << hiScore;
                        outputFile.close();
                    selectOneSound.play();
                    state = State::GAME_OVER;
                }

                if (state == State::PLAYING)
                switch(event.key.code) {
                    case sf::Keyboard::W:
                        yPos -= 32;
                        mainView.setCenter(xPos,yPos);
                        break;
                    case sf::Keyboard::S:
                        yPos += 32;
                        mainView.setCenter(xPos,yPos);
                        break;
                    case sf::Keyboard::A:
                        xPos -= 32;
                        mainView.setCenter(xPos,yPos);
                        break;
                    case sf::Keyboard::D:
                        xPos += 32;
                        mainView.setCenter(xPos,yPos);
                        break;
                    case sf::Keyboard::Up:
                        if (pointedTile > (fieldSize) - 1) pointedTile -= fieldSize;
                        selectTwoSound.play();
                        break;
                    case sf::Keyboard::Down:
                        if (pointedTile < (fieldSize * (fieldSize - 1))) pointedTile += fieldSize;
                        selectTwoSound.play();
                        break;
                    case sf::Keyboard::Left:
                        if (pointedTile > 0) pointedTile -= 1;
                        selectTwoSound.play();
                        break;
                    case sf::Keyboard::Right:
                        if (pointedTile < (fieldSize * fieldSize) - 1) pointedTile += 1;
                        selectTwoSound.play();
                        break;
                    case sf::Keyboard::Return:
                        if (pointedTile != (fieldSize * fieldSize) / 2
                            && tiles[pointedTile].getType() < 13 &&
                            pointedTile != selectedTile) {
                            // Tile matching logic
                            if ((selectedTile + pointedTile == pointedTile &&
                                (selectedTile + pointedTile) % fieldSize != 0) ||
                                (selectedTile - pointedTile == pointedTile &&
                                (selectedTile - pointedTile) % fieldSize != fieldSize - 1) ||
                                selectedTile + fieldSize == pointedTile ||
                                selectedTile - fieldSize == pointedTile) {
                                if (tiles[pointedTile].getType() == tiles[selectedTile].getType() &&
                                    tiles[selectedTile].getType() >= 7) {
                                    tiles[pointedTile].setType(tiles[selectedTile].getType());
                                    tiles[selectedTile].setType(0);
                                    targetScore += 20 * scoreChain;
                                    scoreChain++;
                                    matchSound.play();
                                }
                                else if (tiles[pointedTile].getType() < 7) {
                                    tiles[pointedTile].setType(tiles[selectedTile].getType());
                                    if (nextTileType < 7) moveSound.play();
                                    else dropSound.play();
                                    tiles[selectedTile].setType(nextTileType);
                                    targetScore -= 10;
                                    nextTileType = rand() % 13;
                                }
                            }
                            else scoreChain = 1;
                            selectedTile = pointedTile;
                            break;
                        }
                        break;
                    default:
                        break;
                }
            }

            // Handle controls while playing
            if (state == State::PLAYING)
            {
                // Mouse click event
                if (event.type == event.MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
                    for (int i = 0; i < (fieldSize * fieldSize); i++) {
                        // Selecting a tile (but not person in middle, itself or white tiles)
                        if (tiles[i].getSprite().getGlobalBounds().contains(mouseWorldPosition))
                        if (i != (fieldSize * fieldSize) / 2 && tiles[i].getType() < 13 && i != selectedTile) {
                            selectTwoSound.play();
                            // Tile matching logic
                            if ((selectedTile + 1 == i && (selectedTile + 1) % fieldSize != 0) ||
                                (selectedTile - 1 == i && (selectedTile - 1) % fieldSize != fieldSize - 1) ||
                                selectedTile + fieldSize == i ||
                                selectedTile - fieldSize == i) {
                                if (tiles[i].getType() == tiles[selectedTile].getType() &&
                                    tiles[selectedTile].getType() >= 7) {
                                    tiles[i].setType(tiles[selectedTile].getType());
                                    tiles[selectedTile].setType(0);
                                    targetScore += 20 * scoreChain;
                                    scoreChain++;
                                    matchSound.play();
                                }
                                else if (tiles[i].getType() < 7) {
                                    tiles[i].setType(tiles[selectedTile].getType());
                                    if (nextTileType < 7) moveSound.play();
                                    else dropSound.play();
                                    tiles[selectedTile].setType(nextTileType);
                                    targetScore -= 10;
                                    nextTileType = rand() % 13;
                                }
                            }
                            else scoreChain = 1;
                            selectedTile = i;

                            // Check for no available moves
                            /*for (int i = 1; i < (fieldSize * fieldSize) - 1; i++) {
                                if (tiles[i].getType() < 7 || tiles[i].getType() >= 13) continue;
                                if ((tiles[i - 1].getType() == tiles[i].getType()) ||
                                    (tiles[i - fieldSize].getType() == tiles[i].getType()) ||
                                    (tiles[i + 1].getType() == tiles[i].getType()) ||
                                    (tiles[i + fieldSize].getType() == tiles[i].getType())) break;
                                else state = State::GAME_OVER;
                            }*/
                            break;
                        }
                    }



                    // Check to see if this is a winning move

                }
            }

            // Handle the player quitting
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        // Handle the levelling up state
        if (state == State::LEVEL_UP)
        {
            // Handle the player levelling up
            switch(event.key.code) {
                case sf::Keyboard::Num1:
                    fieldSize = 3;
                    state = State::PLAYING;
                    break;
                case sf::Keyboard::Num2:
                    fieldSize = 9;
                    state = State::PLAYING;
                    break;
                case sf::Keyboard::Num3:
                    fieldSize = 11;
                    state = State::PLAYING;
                    break;
                case sf::Keyboard::Num4:
                    fieldSize = 15;
                    state = State::PLAYING;
                    break;
                case sf::Keyboard::Num5:
                    fieldSize = 19;
                    state = State::PLAYING;
                    break;
                default:
                    break;
            }

            if (state == State::PLAYING)
            {
                // Increase the field size
                fieldSize += 2;

                // Prepare thelevel
                field.width = fieldSize;
                field.height = fieldSize;
                field.left = 0;
                field.top = 0;

                pointedTile = ((fieldSize * (fieldSize + 2)) / 2);
                selectedTile = 0 - (fieldSize * fieldSize);
                nextTileType = rand() % 14;

                // Delete the previously allocated memory (if it exists)
                delete[] tiles;
                tiles = createField(field);
                xPos = fieldSize * 16;
                yPos = fieldSize * 16;
                mainView.setCenter(xPos,yPos);
            }

        }// End levelling up

        // Handle the menu state
        if (state == State::MENU)
        {
            // Mouse click event
                if (event.type == event.MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
                    if (quitText.getGlobalBounds().contains(mouseWorldPosition)) state = State::GAME_OVER;
                }
        }

        /*
         ****************
         UPDATE THE FRAME
         ****************
         */

        if (state == State::PLAYING)
        {
            // Where is the mouse pointer
            mouseScreenPosition = sf::Mouse::getPosition(window);

            // Convert mouse position to world coordinates of mainView
            mouseWorldPosition = window.mapPixelToCoords(mouseScreenPosition, mainView);

            // Update the tiles
            for (int i = 0; i < (fieldSize * fieldSize); i++) {
                tiles[i].update();
            }

            // Reaching the target score
            if (score < targetScore) {
                    score++;
                    if (score > hiScore) hiScore = score;
            }
            if (score > targetScore) score--;

            // Cap the score at 0-999,999
            if (score < 0) score = 0;
            if (score > 999999) score = 999999;

            // Cap the chain at 20
            if (scoreChain > 20) scoreChain = 20;

            // Update the score text
            std::stringstream ssScore;
            ssScore << "Score: " << score;
            scoreText.setString(ssScore.str());

            // Update the score text
            std::stringstream ssChain;
            ssChain << "Chain: x" << scoreChain;
            chainText.setString(ssChain.str());

            // Aesthetic color for chain text
            switch(scoreChain){
                case 0:
                    chainText.setFillColor(sf::Color::White);
                    break;
                case 6:
                    chainText.setFillColor(sf::Color::Yellow);
                    break;
                case 11:
                    chainText.setFillColor(sf::Color::Magenta);
                    break;
                case 16:
                    chainText.setFillColor(sf::Color::Cyan);
                    break;
            }

            switch(nextTileType){
                case 7:
                    nextTile.setColor(sf::Color::Red);
                    break;
                case 8:
                    nextTile.setColor(sf::Color::Yellow);
                    break;
                case 9:
                    nextTile.setColor(sf::Color::Green);
                    break;
                case 10:
                    nextTile.setColor(sf::Color::Cyan);
                    break;
                case 11:
                    nextTile.setColor(sf::Color::Blue);
                    break;
                case 12:
                    nextTile.setColor(sf::Color::Magenta);
                    break;
                default:
                    nextTile.setColor(sf::Color::Black);
                    break;
            }

            // Set position of tile selectors
            for (int i = 0; i < (fieldSize * fieldSize); i++) {
                if(selectedTile == i) {
                        selectSprite.setPosition(tiles[i].getSprite().getPosition());
                }
                if(pointedTile == i) {
                        pointSprite.setPosition(tiles[i].getSprite().getPosition());
                }
            }
        }

        if (state == State::GAME_OVER) {
            // Update the score text
            std::stringstream ssHiScore;
            ssHiScore << "High score: " << hiScore;
            hiScoreText.setString(ssHiScore.str());
        }

        /*
         **************
         Draw the scene
         **************
         */

        if (state == State::PLAYING)
        {
            window.clear();

            // set the mainView to be displayed in the window
            // And draw everything related to it
            window.setView(mainView);

            // Draw the tiles
            for (int i = 0; i < (fieldSize * fieldSize); i++) {
                if(tiles[i].isActive()) window.draw(tiles[i].getSprite());
            }

            window.draw(pointSprite);
            window.draw(selectSprite);

            // Switch to the HUD view
            window.setView(hudView);

            // Draw HUD elements
            window.draw(scoreRect);
            window.draw(scoreText);
            window.draw(chainText);
            window.draw(nextRect);
            window.draw(nextText);
            window.draw(nextTile);
        }

        if (state == State::MENU)
        {
            // Switch to the menu view
            window.setView(menuView);

            // Draw menu
            window.draw(menuRect);
            window.draw(menuText);
            window.draw(quitText);
        }

        if (state == State::GAME_OVER)
        {
            window.clear();

            // Switch to the menu view
            window.setView(menuView);

            window.draw(gameOverText);
            window.draw(hiScoreText);
        }

        window.display();
    }
    delete[] tiles;

    return 0;
}

void moveTiles(int tile) {

}
